﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/1/17
             * Mad Libs Assignment
             * */
            Console.Clear();

            int num1;
            int num2;
            int num3;

            Console.WriteLine("Enter in any three numbers between 1 and 12 and press enter after each one.");
            string num1string = Console.ReadLine();
            while (!(int.TryParse(num1string, out num1))||(num1<1 || num1>12))
            {
                Console.WriteLine("Please only enter a number from 1 to 12.");
                num1string = Console.ReadLine();
            }

            string num2string = Console.ReadLine();
            while (!(int.TryParse(num2string, out num2)) || (num2 < 1 || num2 > 12))
            {
                Console.WriteLine("Please only enter a number from 1 to 12.");
                num2string = Console.ReadLine();
            }

            string num3string = Console.ReadLine();
            while (!(int.TryParse(num3string, out num3)) || (num3 < 1 || num3 > 12))
            {
                Console.WriteLine("Please only enter a number from 1 to 12.");
                num3string = Console.ReadLine();
            }




            Console.WriteLine("Please enter your name.");
            string name=Console.ReadLine();
            while(string.IsNullOrWhiteSpace(name)){
                Console.WriteLine("Please enter your name. Dont leave this blank.");
                name = Console.ReadLine();
            }

            Console.WriteLine("Enter any verb.");
            string verb = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(verb))
            {
                Console.WriteLine("Enter any verb. Dont leave this blank.");
                verb = Console.ReadLine();
            }

            Console.WriteLine("Enter your favorite color.");
            string color = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(color))
            {
                Console.WriteLine("Enter your favorite color. Dont leave this blank.");
                color = Console.ReadLine();
            }

            Console.WriteLine("Enter a random noun.");
            string noun = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(noun))
            {
                Console.WriteLine("Enter a random noun. Dont leave this blank.");
                noun = Console.ReadLine();
            }

            Console.WriteLine("\r\n\r\n"+"At "+num2+" o'clock "+name+" "+verb+" on top of "+num1+" "+ color +" "+noun+". "+name+" broke "+num3+" of them.");

        }
    }
}
