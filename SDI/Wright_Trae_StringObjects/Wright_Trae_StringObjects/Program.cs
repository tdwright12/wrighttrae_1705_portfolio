﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/15/17
             * String Objects*/
            Console.Clear();


            Console.WriteLine("PROBLEM ONE: Email Checker");
            Console.WriteLine("\r\nPlease enter your email address.");
            string email = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(email))
            {
                Console.WriteLine("\r\nPlease do not leave it blank.\r\nEnter your email address.");
                Console.ReadLine();
            }
            Boolean valid = validate(email);
            if (valid == true)
            {
                Console.WriteLine("The email address of {0} is a valid email address.",email);
            }
            else
            {
                Console.WriteLine("The email address of {0} is NOT a valid email address.", email);
            }

            /*test@fullsail.com....VALID
             * test@full@sail.com....NOT VALID
             * test@full sail.com....VALID
             * test@full.sail.com....NOT VALID
            */
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Problem 2:Separator
            Console.WriteLine("\r\n\r\nProblem 2: Separator\r\n");
            Console.WriteLine("Please enter a list of items seperated by seperator.Ex(/,.|)");
            string list = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(list))
            {
                Console.WriteLine("\r\nPlease do not leave this blank.\r\nEnter a list of items seperated by seperator.Ex(/,.|)");
                list = Console.ReadLine();
            }

            Console.WriteLine("\r\nPlease enter the separator you used above.");
            string sep = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(sep))
            {
                Console.WriteLine("Please dont leave this blank.\r\nEnter the separator you used.");
                sep = Console.ReadLine();
            }



            Console.WriteLine("\r\nEnter the separator you would like to replace the old one with.");
            string newSep = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(newSep))
            {
                Console.WriteLine("Dont leace this blank.\r\nEnter the separator you would like to replace the old one with.");
                newSep = Console.ReadLine();
            }



            string newList = Replacer(list, Convert.ToChar(sep),Convert.ToChar(newSep));
            Console.WriteLine("The original string of {0} with the new seperator is {1}.",list,newList);

            /*test1...1-2-3-4-5
             * test2...red,blue,green,pink
             * test3...in-5\1\3\5...out 5.1.3.5*/


        }

        public static Boolean validate(string email)
        {
            Boolean valid=true;
            int atIndex = email.IndexOf("@");
            string noSpace = email.Trim();
            if (email.IndexOf("@")!=email.LastIndexOf("@")|| atIndex == -1) 
            {
                valid = false;
            }else if (!(noSpace.Length==email.Length))
            {
                valid = false;
            }else if (atIndex == -1 || email.IndexOf(".", atIndex) != email.LastIndexOf("."))
            {
                valid = false;
            }
            return valid;
        }

        //////////2


        public static string Replacer(string list, char sep, char newSep)
        {
            string[] aList = list.Split(sep);
            string newList="";
            int count=0;
            foreach (string temp in aList)
            {
                
                count = count + 1;
                if (count < aList.Length)
                {
                    Console.WriteLine(count);
                    newList += temp + newSep;
                }else
                {
                    newList += temp;
                }
                
            }

            return newList;
        }



       
    }
}
