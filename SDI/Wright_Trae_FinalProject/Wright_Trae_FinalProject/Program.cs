﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/24/17
             * Final Project
             * SDI*/
            Console.Clear();
            Counter CaloriesCount = new Counter();
            Console.WriteLine("Hi, this program's job is to count your calories.\r\n\r\nPlease enter the number of meals you have ate today.");
            string stringNumMeals=Console.ReadLine();
            int numMeals;
            while (!(int.TryParse(stringNumMeals,out numMeals)))
            {
                Console.WriteLine("\r\nOnly use numbers when typing this in. Please enter the number of meals you have ate today.");
                stringNumMeals = Console.ReadLine();
            }
            int[] arrayMeals=CaloriesCount.GetCalories(numMeals);
            int totalCalories = CaloriesCount.TotalCalories(arrayMeals);

            Console.WriteLine("What was the total amount of calories you wanted to eat today.");
            string stringGoalCalories = Console.ReadLine();
            int goalCalories;
            while (!(int.TryParse(stringGoalCalories, out goalCalories)))
            {
                Console.WriteLine("What was the total amount of calories you wanted to eat today. Please only enter in a number.");
                stringGoalCalories = Console.ReadLine();

            }

            int leftOverCalories = CaloriesCount.CaloriesLeft(goalCalories, totalCalories);

            if (leftOverCalories < 0)
            {
                Console.WriteLine("You have over eaten by {0} calories today.",leftOverCalories*-1);
            }else
            {
                Console.WriteLine("You still have {0} calories left to consume today.",leftOverCalories);
            }

            /*test 1- Meals:800,1000,1200. Goal:2500. Leftover: -500
             * test 2- Meals:750,1075,1130. Goal:3000. Leftover:+45
             * test 3- Meals:200,2000,1200,1111. Goal:2750. Leftover:-1761
             * 
             * 
             * 
             * */

        }
    }
}
