﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_FinalProject
{
    class Counter
    {


        public int[] GetCalories(int _numMeals)
        {
            int[] meals = new int[_numMeals];
            for (int i=1; i <= _numMeals; i++)
            {
                Console.WriteLine("\r\nPlease enter in the number of calories for meal number {0}.",i);
                int numCalories;
                string stringCalories= Console.ReadLine();
                while (!(int.TryParse(stringCalories,out numCalories)))
                {
                    Console.WriteLine("\r\nPlease re-enter in the number of calories for meal number {0}. Only enter this in number format.", i);
                    stringCalories = Console.ReadLine();
                }

                meals[i-1] = numCalories;


            }


            return meals;
        }


        public int TotalCalories(int[] _meals)
        {
            int totalC=0;
            for (int i = 0; i < _meals.Length; i ++)
            {
               totalC += _meals[i];
            }
            return totalC;
        }

        public int CaloriesLeft(int _goal,int _total)
        {
            int leftOver = _goal -_total;
            return leftOver;

        }

    }
}
