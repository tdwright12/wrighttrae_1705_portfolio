﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/1/17
             * Restaurant Tip Calculator*/
            Console.Clear();
            Console.WriteLine("\r\nWhen you have finished entering the amount please press ENTER.");

            string price1String;
            string price2String;
            string price3String;
            decimal price1;
            decimal price2;
            decimal price3;
            string tipPercentString;
            decimal tipPercent;
            decimal totalWithoutTip;
            decimal totalWithTip;
            decimal threeHalves;
            
            //ask for prices
            Console.WriteLine("\r\nEnter the price of your first meal.(without the dollar sign)");
            price1String = Console.ReadLine();
            while(!(decimal.TryParse(price1String, out price1)))
            {
                Console.WriteLine("Enter the price of your first meal.(without the dollar sign)\r\nPlease only enter a number");
                price1String = Console.ReadLine();
            }

            Console.WriteLine("\r\nEnter the price of your second meal.");
            price2String = Console.ReadLine();
            while (!(decimal.TryParse(price2String, out price2)))
            {
                Console.WriteLine("Enter the price of your second meal.(without the dollar sign)\r\nPlease only enter a number");
                price2String = Console.ReadLine();
            }

            Console.WriteLine("\r\nEnter the price of your final meal.");
            price3String = Console.ReadLine();
            while (!(decimal.TryParse(price3String, out price3)))
            {
                Console.WriteLine("Enter the price of your final meal.(without the dollar sign)\r\nPlease only enter a number");
                price3String = Console.ReadLine();
            }


            totalWithoutTip = price1 + price2 + price3;

            Console.WriteLine("\r\nThe total of your bill is $" + totalWithoutTip + " before the tip.\r\n");

            Console.WriteLine("How much would you like to tip based on your service here?(Please do not include the \"%\" sign)");
            tipPercentString = Console.ReadLine();
            while (!(decimal.TryParse(tipPercentString, out tipPercent)))
            {
                Console.WriteLine("How much would you like to tip based on your service here?(Please do not include the \"%\" sign)\r\nPlease only enter a number.");
                tipPercentString = Console.ReadLine();
            }


            tipPercent = tipPercent * .01m;

            //calculate the tip
            decimal tip = totalWithoutTip * tipPercent;
            totalWithTip = totalWithoutTip + tip;
            //divide the total bill into three equal parts
            decimal totalRounded = Math.Round(totalWithTip, 2);
            threeHalves = totalWithTip / 3;


            Console.WriteLine("The total tip is " + tip+".");
            Console.WriteLine("Your final total with tip included is $" + totalRounded +". Each person will need to pay $"+ threeHalves +".\r\n\r\nThank you have a nice day!");


            /*First Meal=10.00
             * second meal=15
             * third meal=25
             * total before tip=50
             * tip=%20
             * total tip=10
             * final total=60
             * Thirds=20
             * 
             * First Meal=20.25
             * second meal=17.75
             * third meal=23.90
             * total before tip=61.90
             * tip=10
             * total tip=6.19
             * final total=68.09
             * Thirds=22.69666
             * */

        }
    }
}
