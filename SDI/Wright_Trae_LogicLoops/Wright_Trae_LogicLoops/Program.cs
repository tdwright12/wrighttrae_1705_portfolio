﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {/*Trae Wright
            3/6/17
            Logic Loops Problems
            */
            Console.Clear();
            Console.WriteLine("Problem 1– Logical Operators: Tire Pressure\r\n"); 


            int[] tirePressure = new int[4];
            string temp;

            Console.WriteLine("Enter the Pressure of your front left tire and press enter.");
            temp = Console.ReadLine();

            while (!(int.TryParse(temp, out tirePressure[0])))
            {
                Console.WriteLine("Enter the Pressure of your front left tire and press enter. Please only enter a number.");
                temp = Console.ReadLine();
            }

            

            Console.WriteLine("Enter the Pressure of your front right tire and press enter.");
            temp = Console.ReadLine();

            while (!(int.TryParse(temp, out tirePressure[1])))
            {
                Console.WriteLine("Enter the Pressure of your front right tire and press enter. Please only enter a number.");
                temp = Console.ReadLine();
            }

            Console.WriteLine("Enter the Pressure of your back left tire and press enter.");
            temp = Console.ReadLine();

            while(!(int.TryParse(temp, out tirePressure[2])))
            {
                Console.WriteLine("Enter the Pressure of your back left tire and press enter. Please only enter a number.");
                temp = Console.ReadLine();
            }

            Console.WriteLine("Enter the Pressure of your back right tire and press enter.");
            temp = Console.ReadLine();
            while(!(int.TryParse(temp, out tirePressure[3])))
            {
                Console.WriteLine("Enter the Pressure of your back right tire and press enter. PLease only enter a number.");
                temp = Console.ReadLine();
            }


            if (tirePressure[0] == tirePressure[1] && tirePressure[2] == tirePressure[3])
            {
                Console.WriteLine("The tires pass spec!");
            }
            else
            {
                Console.WriteLine("Get your tires checked out!");

            }

            /*Front	Left– 32,Front Right– 32,Back Left-30,Back Right-30- Tires OK
              Front	Left– 36, Front Right– 32, Back Left-25, Back Right- 25- Check Tires
              Front	Left– 33, Front Right– 31, Back Left-26, Back Right- 24- Check Tires
              */

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            
            Console.WriteLine("\r\n\r\nProblem 2-Logical Operators:Movie Ticket Price\r\n");

            Console.WriteLine("Please enter your age and then press enter.");
            string ageString = Console.ReadLine();
            int age;

            while (!(int.TryParse(ageString, out age)))
            {
                Console.WriteLine("Please enter your age and then press enter. Please enter a NUMBER.");
                ageString = Console.ReadLine();
            }
                

            Console.WriteLine("What time would you like to see the movie.");
            string timeString = Console.ReadLine();
            int time;
            while (!(int.TryParse(timeString, out time)))
            {
                Console.WriteLine("What time would you like to see the movie. PLease enter in a number.");
                timeString = Console.ReadLine();
            }

            decimal price = 12.00m;

            if (age >= 55 || age <= 10 || (time >= 14 && time <= 17))
            {
                price = 7.00m;

            }
            Console.WriteLine("The ticket price is " + price + ".");

            /*Age– 57, Time	– 20, Ticket Price - $7.00
              Age– 9, Time	– 20, Ticket Price - $7.00
              Age– 38, Time	– 20, Ticket Price - $12.00
              Age– 25, Time	– 16, Ticket Price - $7.00
              Age– 1, Time	– 24, Ticket Price - $7.00
              */

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            Console.WriteLine("\r\n\r\nProject 3-For Loop: Add Up The Odds or Evens\r\n");


            int[] num = new int [7] {2,10,50,3,21,5,12};
            int total=0;

            Console.WriteLine("Do you want to see the total of the odd or even numbers?");
            string EorO = Console.ReadLine();

            while (EorO.ToLower() != "odd"&&EorO.ToLower() != "even")
            {
                Console.WriteLine("Do you want to see the total of the odd or even numbers? Please only enter in odd or even.");
                EorO = Console.ReadLine();
            }
            
            for (int i = 0; i<=6;i++)
            {
                if (num[i] % 2 == 0 && EorO.ToLower() == "even")
                {
                    total = total + num[i];
                }else if(num[i]%2!=0 && EorO.ToLower() == "odd")
                {
                    total = total + num[i];
                }
            }
            
            if (EorO == "even")
            {
                Console.WriteLine("The total of the even numbers are " + total);
            }else
            {
                Console.WriteLine("The total of the odd numbers are " + total);
            }

            /*Array{1,3,4,5,7,13,14}, sum of evens - 18 sum of odds - 29
             * Array{2,10,50,3,21,5,12}, sum of evens - 74 sum of odds - 29
             * */

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("\r\n\r\nProblem #4 – While Loop: Charge It!\r\n"); 

            Console.WriteLine("Please enter in your max credit limit.");
            string maxCredString = Console.ReadLine();
            decimal maxCred;
            while(!(decimal.TryParse(maxCredString, out maxCred)))
            {
                Console.WriteLine("Please enter in your max credit limit. Make sure to enter a number.");
                maxCredString = Console.ReadLine();
            }
            
            string purchaseString="";
            decimal purchase;
            

            while (maxCred > 0)
            {
                Console.WriteLine("Please enter the amount of your purchase.");
                purchaseString = Console.ReadLine();
                while (!(decimal.TryParse(purchaseString,out purchase))){
                    Console.WriteLine("Please enter the amount of your purchase.");
                    purchaseString = Console.ReadLine();
                }
                maxCred = maxCred - purchase;
                if (maxCred <= 0)
                {
                    Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by ${0}.", maxCred);
                        
                }
                else
                {
                    Console.WriteLine("\r\nWith your current purchase of ${0}, you can still spend ${1}.",purchase,maxCred );
                }
                

            }

            
                
                    
                
            /*credit limit-500.21
             *purchase 1- 200.90
             * purchase 2-100
             * purchase 3-3.77
             * purchase 4-200.11
             * I exceeded my limit by 4.57
             */






        }
    }
}
