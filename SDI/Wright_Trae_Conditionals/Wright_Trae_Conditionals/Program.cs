﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/3/13
             * Conditionals*/
            Console.Clear();
            
            Console.WriteLine("Project 1\r\nPlease enter in the number of the temperature.");
            string tempString = Console.ReadLine();
            double temp;
            while(!(double.TryParse(tempString, out temp)))
            {
                Console.WriteLine("Please enter in the number of the temperature. Please only enter a number.");
                tempString = Console.ReadLine();
            }



            Console.WriteLine("You've entered in the number {0}. Is that number measued in C(Celsius) or F(Fahrenheit).",temp);
            string degree = Console.ReadLine();

            while (degree.ToLower() != "c" && degree.ToLower() !="f")
            {
                Console.WriteLine("Please enter in if its measued in C(Celsius) or F(Fahrenheit). Please only enter in either a \"c\" or a \"f\".");
                degree = Console.ReadLine();
            }

            if (degree.ToLower() == "c")
            {
                temp = temp * 9 / 5 + 32;
                Console.WriteLine("The temperature you put is " + temp + " degrees Fahrenhiet");
            }else if (degree.ToLower() == "f")
            {
                temp = (temp - 32) * 5 / 9;
                Console.WriteLine("The temperature you put is " + temp + " degrees Celsius");
            }
            /*32F is 0C, WORKS
             * 100C is 212F, WORKS
             * 50 c, Does the lowercase c make a differance? No and the temperature is 122F.
             * 80F is 26.6666C, WORKS
             * */

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("\r\n\r\nProject 2\r\n");
            double gallons;
            double gasTank;
            double mpg;
            double miles;

            Console.WriteLine("How many gallons does your car tank hold?");
            string gallonsString = Console.ReadLine();

            while (!(double.TryParse(gallonsString, out gallons)))
            {
                Console.WriteLine("How many gallons does your car tank hold?");
                gallonsString = Console.ReadLine();
            }

            Console.WriteLine("How full is your gas tank?(in %)");
            string gasTankString = Console.ReadLine();

            while (!(double.TryParse(gasTankString, out gasTank))){
                Console.WriteLine("How full is your gas tank?(in %)");
                gasTankString = Console.ReadLine();
            }

            Console.WriteLine("How many miles per gallon does your car go?");
            string mpgString = Console.ReadLine();

            while (!(double.TryParse(mpgString, out mpg)))
            {
                Console.WriteLine("How many miles per gallon does your car go?");
                mpgString = Console.ReadLine();
            }

            
            
            
            
            gasTank = gasTank * .01;
            gallons = gallons * gasTank;//get the reamining amount of gallons
            miles = gallons * mpg;

            if (miles >= 200)
            {
                Console.WriteLine("Yes, you can drive " + miles + " more miles and you can make it without stopping for gas!");
            }
            else
            {
                Console.WriteLine("You only have " + miles + " gallons of gas in your tank, better get gas now while you can!");
            
            }

            /*Gallons-20, Gas Tank-50%, MPG-25. You can make it
             * Gallons-12, Gas Tank-60%, MPG- 20.You cant make it
             * Gallons-35, Gas Tank-90%, MPG-15.Yes, you can drive 472.5 more miles and you can make it without stopping for gas!*/

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("\r\n\r\nProject 3\r\n");

            int gradePer;
            string grade="";

            Console.WriteLine("Enter your grade.(in %)");
            string gradePerString = Console.ReadLine();
            while(!(int.TryParse(gradePerString, out gradePer)))
            {
                Console.WriteLine("Enter your grade.(in %) Please only enter a number");
                gradePerString = Console.ReadLine();
            }

            while (gradePer > 100 || gradePer < 0)
            {
                Console.WriteLine("The number you entered is either too high or too low, please enter a grade from 100-0.(in %)");
                gradePerString = Console.ReadLine();
                int.TryParse(gradePerString, out gradePer);

            }

            

            if (gradePer >=90&&gradePer<=100)
            {
                grade = "A";
            }else if (gradePer >= 80 && gradePer <= 89)
            {
                grade = "B";
            }else if (gradePer >= 73 && gradePer <= 79)
            {
                grade = "C";
            } else if (gradePer >= 70 && gradePer <= 72)
            {
                grade = "D";
            }else if (gradePer >= 0 && gradePer <= 69)
            {
                grade = "F";
            }

            Console.WriteLine("You have a " + gradePer + "%, which means you have a(n) " + grade + " in the class!");

            /*Grade-92%... earned an a
             * Grade-80%...earned a b
             * grade-67%...earned a f
             * When you type in 120% you are re-prompted, becuase it is an invalid answer
             * grade-71%...earned a d*/

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("\r\n\r\nProject 4\r\n");


            decimal firstMeal;
            decimal secondMeal;
            string discount = "";



            Console.WriteLine("Please enter the cost of your first item and press enter.");
            string firstMealString = Console.ReadLine();

            while(!(decimal.TryParse(firstMealString, out firstMeal)))
            {
                Console.WriteLine("Please enter the cost of your first item and press enter.");
                firstMealString = Console.ReadLine();
            }

            Console.WriteLine("Enter the price of your second meal.");
            string secondMealString = Console.ReadLine();

            while(!(decimal.TryParse(secondMealString, out secondMeal))){
                Console.WriteLine("Enter the price of your second meal.");
                secondMealString = Console.ReadLine();
            }
           
            
            
            decimal total = firstMeal + secondMeal;
            

            if (total >= 100)
            {
                total = total * .9m;
                discount = "10";
                Console.WriteLine("Your total purchase is $" + total + " which includes your "+discount+"% discount.");
            }
            else if (total >= 50 && total <= 100)
            {
                total = total * .95m;
                discount = "5";
                Console.WriteLine("Your total purchase is $" + total + " which includes your " + discount + "% discount.");
            }else
            {
                Console.WriteLine("Your total purchase is $" + total + ".");
            }

            

            /*test 1- total 108.45, discount 10
             * test 2-total 52.25, discount 5
             * test 3-total 18.25, no discount
             * test 4-total 43.25, no discount*/


        }
    }
}
