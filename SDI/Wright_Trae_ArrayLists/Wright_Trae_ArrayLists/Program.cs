﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/13/17
             * array lists*/
             Console.Clear();
            NameArrayList();




        }
        public static void NameArrayList()
        {
            Console.Clear();
            ArrayList names = new ArrayList { "Lisa", "Greg", "Lacy", "Maryann", "Logan" };
            ArrayList family = new ArrayList { "mom", "dad", "sister", "girlfriend", "step-brother" };

            for (int i = 0; i < names.Count; i++)
            {
                Console.WriteLine("{0} is my {1}.",names [i],family [i]);
            }

            names.Remove("Logan");
            family.Remove("step-brother");
            names.Remove("Lacy");
            family.Remove("sister");
            names.Insert(0,"Maddox");
            family.Insert(0, "nephew");



            Console.WriteLine("\r\n");

            for (int i = 0; i < names.Count; i++)
            {
                Console.WriteLine("{0} is my {1}.", names[i], family[i]);
            }


        }
    }
}
