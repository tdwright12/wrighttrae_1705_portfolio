﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Trae Wright
             Day 3
             3/3/17
             Arrays Assignment
             */

            Console.Clear();
            //Declare and Define The Starting Number Arrays
            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //Find the total of each array and store it in a variable and output to console
            int firstArrayTotal = firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3];
            double secondArrayTotal = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];
            Console.WriteLine("The total of the first array is " + firstArrayTotal + " and the second array's total is " + secondArrayTotal);

            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.
            decimal firstArrayAver = (firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3]) / 4;
            double secondArrayAver = (secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3]) / 4;
            Console.WriteLine("\r\nThe average of the first array is " + firstArrayAver + " and the second array's average is " + secondArrayAver);

            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */
            double[] thirdArray = new double[4] { firstArray[0] + secondArray[0], firstArray[1] + secondArray[1], firstArray[2] + secondArray[2], firstArray[3] + secondArray[3] };
            Console.WriteLine("The numbers for the third array are " + thirdArray[0] + ", " + thirdArray[1] + ", " + thirdArray[2] + ", " + thirdArray[3]);




            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };
            string sentence = (MixedUp[1]+" "+MixedUp[3]+" "+MixedUp[4]+" "+MixedUp[0]+" "+MixedUp [2]);
            Console.WriteLine(sentence);


        }
    }
}
