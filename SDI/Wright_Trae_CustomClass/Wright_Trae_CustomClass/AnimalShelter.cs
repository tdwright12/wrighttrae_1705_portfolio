﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_CustomClass
{
    class AnimalShelter
    {
        int mhighNum;
        int mlowNum;
        int mcurrentNum;


        public AnimalShelter(int _high,int _low,int _current)
        {
            mhighNum = _high;
            mlowNum = _low;
            mcurrentNum = _current;
        }

        public int RemoveDog(int _removed)
        {
            if (mcurrentNum - _removed > mlowNum)
            {
                mcurrentNum -= _removed;
            }
            else
            {

                mcurrentNum = mlowNum;
            }
            return mcurrentNum;
        }

        public int GetCurrent()
        {
            return mcurrentNum;
        }

        public int AddDog(int _add)
        {
            if (mcurrentNum + _add < mhighNum)
            {
                mcurrentNum += _add;
            }else
            {
                mcurrentNum = mhighNum;
            }
            return mcurrentNum;
        }





        public int GetHigNum()
        {
            return mhighNum;
        }
        public int GetLowNum()
        {
            return mlowNum;
        }
        public void SetCurrentNum(int _newNum)
        {
            this.mcurrentNum = _newNum;
        }
        public void SetHighNum(int _newNum)
        {
            this.mhighNum = _newNum;
        }
        public void SetLowNum (int _newNum)
        {
            this.mlowNum = _newNum;
        }

    }
}
