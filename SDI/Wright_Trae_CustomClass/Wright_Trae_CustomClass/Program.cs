﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Trae Wright
             *3/20/16
             *Animal Shelter Class*/
            Console.Clear();
            AnimalShelter dogs = new AnimalShelter(5,0,2);

            for (int i = 0; i < 5;i++){
                Console.WriteLine("Did you add or remove dogs?");
                string RorA=Console.ReadLine();
                while(!(RorA.ToLower() == "add"||RorA.ToLower() == "remove"))
                {

                    Console.WriteLine("Did you add or remove dogs?(PLease only enter \"add\" or \"remove\".");
                    RorA = Console.ReadLine();
                }

                Console.WriteLine("How many dogs did you {0}?",RorA);
                string sNumDogs = Console.ReadLine();
                int numDogs;
                while (!(int.TryParse(sNumDogs,out numDogs)))
                {
                    Console.WriteLine("How many dogs did you {0}?Only enter in numbers.", RorA);
                    sNumDogs = Console.ReadLine();
                }
                string end;
                if (RorA.ToLower() == "add")
                {
                    end = "ed";
                    dogs.AddDog(numDogs);
                }else
                {
                    end = "d";
                    dogs.RemoveDog(numDogs);
                }


                int currentDogs = dogs.GetCurrent();


                Console.WriteLine("The new total of dogs are {0} after you {1}{3} {2} dog(s).",currentDogs, RorA,numDogs, end);
                }

        }
    }
}
