﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/10/17
             * Count Fish
             * */
            Console.Clear();

            string[] fColors = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };

            Console.WriteLine("Choose on of the following colors of fish to search for:\r\nPress(1) for RED, (2) for BLUE, (3) for GREEN, (4) for YELLOW");
            string userString = Console.ReadLine();
            while (!(userString == "1"||userString == "2"||userString =="3"||userString == "4"))
            {
                Console.WriteLine("Choose on of the following colors of fish to search for:\r\nPress(1) for RED, (2) for BLUE, (3) for GREEN, (4) for YELLOW.\r\n Please only enter in the numbers 1,2,3,or 4.");
                userString = Console.ReadLine();
            }
            int user;
            int count=0;
            string C;
            int.TryParse(userString, out user);
            if (user == 1){
                C = "red";
            }else if (user == 2)
            {
                C = "blue";
            }else if(user == 3)
            {
                C = "green";
            }else
            {
                C = "yellow";
            }

            foreach (string col in fColors)
            {
                if (col== C)
                {
                    count++;
                }
            }
            Console.WriteLine("In the fish tank there are {0} fish of the color {1}.",count, C);


        }
    }
}
