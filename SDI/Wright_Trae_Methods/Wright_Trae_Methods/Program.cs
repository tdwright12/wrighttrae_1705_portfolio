﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wright_Trae_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Trae Wright
             * 3/10/17
             * Methods*/
            Console.Clear();
            Console.WriteLine("Problem 1:Painting A Wall\r\n");
            Console.WriteLine("PLease enter the width of the wall you are painting and hit enter.");
            string widthString = Console.ReadLine();
            int width;

            while (!(int.TryParse(widthString, out width))){
                Console.WriteLine("PLease enter the width of the wall you are painting and hit enter.");
                widthString = Console.ReadLine();
            }

            Console.WriteLine("Now enter the height.");
            string heightString = Console.ReadLine();
            int height;

            while (!(int.TryParse(heightString, out height))){
                Console.WriteLine("PLease enter the height of the wall you are painting and hit enter.");
                heightString = Console.ReadLine();
            }
            Console.WriteLine("Thank you for entering the width of {0} and the height of {1}.",width, height);

            Console.WriteLine("\r\nEnter the number of coats you are painting?");
            string coatString = Console.ReadLine();
            int coat;
            while (!(int.TryParse(coatString,out coat)))
            {
                Console.WriteLine("Enter the number of coats you are painting?");
                coatString = Console.ReadLine();
            }

            Console.WriteLine("\r\nPlease enter the surface area one gallon of paint will cover in square feet.");
            string surfString = Console.ReadLine();
            int surf;
            while (!(int.TryParse(surfString, out surf)))
            {
                Console.WriteLine("Please enter the surface area one gallon of paint will cover in square feet.");
                surfString = Console.ReadLine();
            }
            Console.WriteLine("\r\nFor {0} coats on that wall, you will need {1} gallons of paint.\r\n\r\n",coat, PaintingAWall(height,width,surf, coat));

            /*W-8, H-10,C-2,Surface A-300......53
             * W-30, H-12.5, Coats-3, Surface A-350.......3.21
             * W-20. H-10, coats-4,Surface A-350.......2.29
             * */

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("Problem 2: Stung!\r\n");
            Console.WriteLine("Please enter the animals weight in pounds.");
            string weightString = Console.ReadLine();
            int weight;
            while (!(int.TryParse(weightString,out weight)))
            {
                Console.WriteLine("Please enter the animals weight in pounds.");
                weightString = Console.ReadLine();

            }
            Console.WriteLine("It takes {0} bee stings to kill this animal.\r\n\r\n",stung(weight));

            /*Weight 10... 90 bee strings
             * Weight 160....1440 bee stings
             * Weight twenty... re promted the user
             * Weight 220....1980 bee stings
             * */






            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Console.WriteLine("Problem 3:\r\n");

            string[] strin = new string[] { "Blue", "Yellow", "Red", "Dog", "Cat" };
            string[]revStrin=ReverseIt(strin);

            Console.Write("Your original array was ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(strin[i]);
                if (i < 4)
                {
                    Console.Write(", ");
                }else
                {
                    Console.Write(" ");
                }
            }



            Console.Write("and now it is reverseed as ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(revStrin[i]);
                if (i < 4)
                {
                    Console.Write(", ");
                }else
                {
                    Console.Write(".\r\n");
                }
            }
            

            /*Input- apple, pear, peach, coconut, kiwi....Output-kiwi, coconut, peach, pear, apple
             * Input-red, orange, yellow, green, blue, indigo, violet.....Output- violet,indigo,blue,green,yellow,orange,red
             */



     


        }



        public static string [] ReverseIt(string[]Str)//#3
        {
            string[] revStr = new string[5];
            int z=0;
            for (int i = Str.Length - 1; i >= 0; i--, z++)
            {
           
                revStr[z] = Str[i];
                
            }
            return revStr;
        }





        public static int stung(int W)//#2
        {
            int max = W * 9;
            return max;
        }












        public static decimal PaintingAWall(int H,int W, int S, int C)//#1
        {
            decimal wallSurf=H*W;
            
            decimal gallons = (wallSurf*C)/S;
            
            gallons= Math.Round(gallons, 2);
            return gallons;
            

        }
    }
}
