﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckerProgram
{
    public class Checker
    {

        public static string String()
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Invalid response. Please don't leave this blank.");
                }
                else
                {
                    return input;
                }
            }
        }

        public static int Int()
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    int num = 0;
                    if (!(int.TryParse(input, out num)))
                    {
                        Console.WriteLine("Invalid response. You did not enter in a NUMBER.");
                    }
                    else
                    {
                        return num;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid response. Please don't leave this blank.");
                }
            }

        }

        public static double Double()
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    double num = 0;
                    if (!(double.TryParse(input, out num)))
                    {
                        Console.WriteLine("Invalid response. You did not enter in a NUMBER.");
                    }
                    else
                    {
                        return num;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid response. Please don't leave this blank.");
                }
            }

        }

        public static int IntRange(int min, int max)
        {
            while (true)
            {
                string input = Console.ReadLine();
                int num = 0;
                if (int.TryParse(input, out num))
                {

                    if (num > max || num < min)
                    {
                        Console.WriteLine("Invalid response. Please only enter in a number from {0} to {1}.", min, max);
                    }
                    else
                    {
                        return num;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid response. You did not enter in a NUMBER.");
                }

            }
        }

        public static string StringRange(int min, int max)
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Invalid response. Please don't leave this blank.");
                }
                else
                {
                    int num = 0;
                    if (int.TryParse(input, out num))
                    {

                        if (num > max || num < min)
                        {
                            Console.WriteLine("Invalid response. Please only enter in a number from {0} to {1}.", min, max);
                        }
                        else
                        {
                            return num.ToString(); ;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid response. You didn't enter in a number.";
                    }

                }
            }
        }

        public static string SpecString(string word1, string word2, string word3, string word4, string word5)// ""means empty
        {
            while (true)
            {
                ArrayList param = new ArrayList { word1.ToLower(), word2.ToLower(), word3.ToLower(), word4.ToLower(), word5.ToLower()};
                while (param.Contains(""))
                {
                    param.Remove("");
                }
                string input = Console.ReadLine().ToLower();
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Invalid response. Please don't leave this blank.");
                }
                else
                {
                    foreach (string row in param)
                    {
                        if (param.Contains(input))
                        {
                            return input;
                        }
                    }
                    for (int i=0; i < param.Count;i++)
                    {
                        if (i == 0)
                        {
                            Console.Write("Please only enter in ");
                        }
                        if (i != param.Count-1)
                        {
                            Console.Write(param[i]+", ");
                        }else
                        {
                            Console.WriteLine(param[i]+".");
                        }
                        
                    }
                    Console.WriteLine("Invalid Response.");
                }
            }
        }



    }
}
