﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class TitleScreen
    {
        public static void swapInfo()
        {
            Console.Clear();
            Console.WriteLine("          _____ _           _ _                         __ ");
            Console.WriteLine(@"         / ____| |         | | |                       /_ |");
            Console.WriteLine(@"        | |    | |__   __ _| | | ___ _ __   __ _  ___   | |");
            Console.WriteLine(@"        | |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \  | |");
            Console.WriteLine(@"        | |____| | | | (_| | | |  __/ | | | (_| |  __/  | |");
            Console.WriteLine(@"         \_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|  |_|");
            Console.WriteLine(@"                                            __/ |          ");
            Console.WriteLine("                                           |___/           \r\n\r\n");
        }

        public static void AgeConvert()
        {
            Console.Clear();
            Console.WriteLine("          _____ _           _ _                           ___  ");
            Console.WriteLine(@"         / ____| |         | | |                         |__ \ ");
            Console.WriteLine(@"        | |    | |__   __ _| | | ___ _ __   __ _  ___     __) |");
            Console.WriteLine(@"        | |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \   |__ < ");
            Console.WriteLine(@"        | |____| | | | (_| | | |  __/ | | | (_| |  __/   ___) |");
            Console.WriteLine(@"         \_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|  |____/ ");
            Console.WriteLine(@"                                            __/ |          ");
            Console.WriteLine("                                           |___/           \r\n\r\n");

        }
        public static void Backwards()
        {
            Console.Clear();
            Console.WriteLine("          _____ _           _ _                          ____  ");
            Console.WriteLine(@"         / ____| |         | | |                        |___ \ ");
            Console.WriteLine(@"        | |    | |__   __ _| | | ___ _ __   __ _  ___      ) |");
            Console.WriteLine(@"        | |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \    / / ");
            Console.WriteLine(@"        | |____| | | | (_| | | |  __/ | | | (_| |  __/   / /_ ");
            Console.WriteLine(@"         \_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|  |____|");
            Console.WriteLine(@"                                            __/ |          ");
            Console.WriteLine("                                           |___/           \r\n\r\n");
        }


        public static void TempConvert()
        {
            Console.Clear();
            Console.WriteLine("          _____ _           _ _                           _____ ");
            Console.WriteLine(@"         / ____| |         | | |                         | ____|");
            Console.WriteLine(@"        | |    | |__   __ _| | | ___ _ __   __ _  ___    | |__  ");
            Console.WriteLine(@"        | |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \   |___ \ ");
            Console.WriteLine(@"        | |____| | | | (_| | | |  __/ | | | (_| |  __/    ___) |");
            Console.WriteLine(@"         \_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|   |____/ ");
            Console.WriteLine(@"                                            __/ |          ");
            Console.WriteLine("                                           |___/           \r\n\r\n");

        }

    }
}
