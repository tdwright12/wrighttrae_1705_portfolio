﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class swapInfo
    {
        //Challenge 1, "SwapInfo".
        //Synopsis:  This program is ment to reverse the information the user inputs.


        public static string getFirstName()
        {

            Console.WriteLine("Please enter your first name, then press enter.");
            string firstName = Console.ReadLine();
            int test;
            while (string.IsNullOrWhiteSpace(firstName)|| int.TryParse(firstName, out test))
            {
                Console.WriteLine("Please enter your first name. You've enterd \"{0}\", please dont leave this blank or use numbers.",firstName);
                firstName = Console.ReadLine();
            }
            return firstName;

        }

        public static string getLastName()
        {
            Console.WriteLine("Please enter your last name.");
            string lastName = Console.ReadLine();
            int test;
            while (string.IsNullOrWhiteSpace(lastName) || int.TryParse(lastName, out test))
            {
                Console.WriteLine("Please enter your last name. You've enterd \"{0}\", please dont leave this blank or use numbers.", lastName);
                lastName = Console.ReadLine();
            }
            return lastName;
        }

        public static void readOut(string _firstName, string _lastName)
        {
            Console.WriteLine(_firstName +" "+ _lastName);
        }

        public static void readOutReverse(string _firstName, string _lastName)
        {
            Console.WriteLine(_lastName +" "+ _firstName);
        }




        public static void run()
        {
            string run = "0";
            while (run != "5")
            {
                TitleScreen.swapInfo();
                string firstName = swapInfo.getFirstName();
                string lastName = swapInfo.getLastName();
                swapInfo.readOut(firstName, lastName);
                swapInfo.readOutReverse(firstName, lastName);
                Console.WriteLine("\r\n\r\nPress 5 to go back to the main menu or press enter to run it again.");
                run = Console.ReadLine();
            }
        }

    }
}
