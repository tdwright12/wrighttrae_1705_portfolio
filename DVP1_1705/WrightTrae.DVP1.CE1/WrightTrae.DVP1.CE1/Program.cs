﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*  Name:  Trae Wright
                Date:  1705
                Course:  Project & Portfolio 1
                Synopsis:  This program is meant to run four different challenges using a menu.*/

            Console.Clear();

            //Challenge 5, "Menu"
            //Synopsis:  Build a menu for the other programs.

            Boolean run = true;
            while (run==true)
            {
                Console.WriteLine("Please enter what program you would like to run.\r\nEnter 1 for \"SwapInfo\"\r\n2 for \"Backwards\"\r\n3 for \"AgeConvert\"\r\n4 for \"TempConvert\"\r\n\r\nPress 5 to exit");
                string program = Console.ReadLine();
                int programNum;

                while (!(int.TryParse(program, out programNum)) || programNum > 5 || programNum < 1)
                {
                    Console.Clear();
                    Console.WriteLine("Oops you entered \"{0}\" please ONLY enter in a NUMBER from 1 to 4.\r\nEnter 1 for \"SwapInfo\"\r\n2 for \"Backwards\"\r\n3 for \"AgeConvert\"\r\n4 for \"TempConvert\"\r\n\r\nPress 5 to exit", program);
                    program = Console.ReadLine();
                }

                Console.Clear();
                if (programNum == 1)
                {
                    Console.WriteLine("Thank you, now running the program \"SwapInfo\"...");
                    Thread.Sleep(2000);
                    swapInfo.run();
                } else if (programNum == 2)
                {
                    Console.WriteLine("Thank you, now running the program \"Backwards\"...");
                    Thread.Sleep(2000);
                    Backwards.run();
                }
                else if (programNum == 3)
                {
                    Console.WriteLine("Thank you, now running the program \"AgeConvert\"...");
                    Thread.Sleep(2000);
                    AgeConvert.run();
                }
                else if (programNum == 4)
                {
                    Console.WriteLine("Thank you, now running the program \"TempConvert\"...");
                    Thread.Sleep(2000);
                    TempConvert.run();
                }else
                {
                    run = false;
                }
                Console.Clear();
            }
        }
    }
}
