﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class AgeConvert
    {
        //Challenge 3, "AgeConvert".
        //Synopsis:  This program prompts the user for their age and name and then converts the age to days and then hours, seconds, and minutes.


        public static string promptName()
        {

                Console.Write("Please enter in your name:");
                string name = Console.ReadLine();
                while (name.Any(char.IsDigit) || string.IsNullOrWhiteSpace(name))
                {
                    Console.Write("Invalid response please only enter in leters and dont leave this blank.\r\nPlease enter in your name:");
                    name = Console.ReadLine();
                }
            return name;

                

        }

        public static int promptAge()
        {
            Console.Write("Please enter in an age to be converted:");
            string ageS = Console.ReadLine();
            int age;
            while (!(int.TryParse(ageS, out age)))
            {
                Console.Write("Invalid response, please only type in numbers.\r\nPlease enter in an age to be converted:");
                ageS = Console.ReadLine();
            }
            return age;
        }


        /*
        public static int leapCalc(int age)
        {
            int leapYear = 2017 - age;
            int yearDiff = 2017 - leapYear;
            int leap = yearDiff / 4;
        }*/

        
        public static int convertDays(int age)
        {
            int ageDays = age * 365;
            return ageDays;
        }

        public static int convertHours(int age)
        {
            int ageHours = age * 365 * 24;
            return ageHours;
        }

        public static int convertMinutes(int age)
        {
            int ageMin = age * 365 * 1440;
            return ageMin;
        }

        public static int convertSec(int age)
        {
            int ageSec = age * 60*365*1440;
            return ageSec;
        }


        public static void run()
        {
            string run = "0";
            while (run != "5")
            {
                TitleScreen.AgeConvert();
                int age = promptAge();
                Console.WriteLine("Hi {5} the age you entered is {0} years old or {1} days old, {2} hours old, {3} minutes old, or {4} seconds old.", age, convertDays(age), convertHours(age), convertMinutes(age), convertSec(age), promptName());
                Console.WriteLine("\r\n\r\nPress 5 to go back to the main menu or press enter to run it again.");
                run = Console.ReadLine();
            }
        }

    }
}
