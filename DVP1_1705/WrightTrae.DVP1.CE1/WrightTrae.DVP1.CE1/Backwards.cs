﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class Backwards
    {
        //Challenge 2, "Backwards".
        //Synopsis:  This program prompts the user for a sentence of at least 6 words and then reverse the order of the sentence.

        public static string[] prompt()
        {
                Console.WriteLine("Please enter a sentence with no less than six words. This program will reverse the order of it.");
                string sent = Console.ReadLine();
                string[] rSent = sent.Split(Convert.ToChar(" "));
                while (string.IsNullOrWhiteSpace(sent) || rSent.Length < 6)
                {
                    Console.WriteLine("Please don't leave this blank.");
                    sent = Console.ReadLine();
                    rSent = sent.Split(Convert.ToChar(" "));
                }
            return rSent;

        }


        public static void swap(string[] _rSent)
        {
            for (int i = _rSent.Length - 1; i >= 0; i--)//reverse the order
            {
                Console.Write(_rSent[i] + " ");
            }
        }


        public static void run()
        {
            string run = "0";
            while (run != "5")
            {
                TitleScreen.Backwards();
                swap(prompt());
                Console.WriteLine("\r\n\r\nPress 5 to go back to the main menu or press enter to run it again.");
                run = Console.ReadLine();
            }
        }




    }
}
