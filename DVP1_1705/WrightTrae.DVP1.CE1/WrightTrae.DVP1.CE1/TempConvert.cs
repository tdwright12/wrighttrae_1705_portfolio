﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrightTrae.DVP1.CE1
{
    class TempConvert
    {
        //Challenge 4, "TempConvert".
        //Synopsis:  This program prompts the user for a temperature in fahrenheit and then celsius and converts them.


            public static double promptTemp()
        {
            Console.WriteLine("Please enter in a temperature to be converted.");
            string tempString = Console.ReadLine();
            double temp;
            while (!(double.TryParse(tempString, out temp)))
            {
                Console.WriteLine("Please enter in the number of the temperature. Please only enter a number.");
                tempString = Console.ReadLine();
            }
            return temp;
        }


        public static string promptDegree(double _temp)
        {
                Console.WriteLine("You've entered in the number {0}. Is that number measued in C(Celsius) or F(Fahrenheit).", _temp);
                string degree = Console.ReadLine();

                while (degree.ToLower() != "c" && degree.ToLower() != "f")
                {
                    Console.WriteLine("Please enter in if its measued in C(Celsius) or F(Fahrenheit). Please only enter in either a \"c\" or a \"f\".");
                    degree = Console.ReadLine();
                }
            return degree;
            
        }


        public static double celsiusConvert(double _temp)
        {
            _temp = (_temp - 32) * 5 / 9;
            Console.WriteLine("The temperature you put is " + _temp + " degrees Celsius");
            return _temp;
        }


        public static double fahrenhietConvert(double _temp)
        {
            _temp = _temp * 9 / 5 + 32;
            Console.WriteLine("The temperature you put is " + _temp + " degrees Fahrenhiet");
            return _temp;
        }

        public static void run()
        {
            string run = "0";
            while (run != "5")
            {
                TitleScreen.TempConvert();
                double temp = promptTemp();
                string deg = promptDegree(temp);
                if (deg.ToLower() == "c")
                {
                    fahrenhietConvert(temp);
                }
                else
                {
                    celsiusConvert(temp);
                }
                Console.WriteLine("\r\n\r\nPress 5 to go back to the main menu or press enter to run it again.");
                run = Console.ReadLine();
            }
        }
    }
}
